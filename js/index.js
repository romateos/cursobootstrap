$(function(){
	$("[data-toggle='tooltip']").tooltip();		//Definir script para el TOOLTIP-->
	$("[data-toggle='popover']").popover();		//Definir script para el POPOVER-->
	$('.carousel').carousel( {interval:2000} );	//Definir opciones para CAROUSEL-->
	
	//Definir EVENTOS (jQuery)-->
	$('#contacto').on('show.bs.modal', function(e){
		console.log('El modal se muestra'); 
		$('#contactoBtn').removeClass('btn-outline-success');
		$('#contactoBtn').addClass('btn-primary');
		$('#contactoBtn').prop('disabled', true);
	});
	$('#contacto').on('shown.bs.modal', function(e){
		console.log('El modal se mostró'); 
	});
	$('#contacto').on('hide.bs.modal', function(e){
		console.log('El modal se oculta'); 
	});
	$('#contacto').on('hidden.bs.modal', function(e){
		console.log('El modal se ocultó'); 
		$('#contactoBtn').prop('disabled', false);
		$('#contactoBtn').removeClass('btn-primary');
		$('#contactoBtn').addClass('btn-outline-success');
	});		
			
});